﻿/*

 The MIT License (MIT)

 Copyright (c) 2015 Sergei Aleksandrov <sergei.a.aleks@gmail.com>

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

*/

/*

 Export for printing (Adobe Illustrator javascript.)

 	Description:
 		Export for printing - Adobe Illustrator script to export files for printing.
 		Export formats: 'pdf' and with bleed, 'png', 'tiff'(experimental)
 	Attention:
 		Tested Adobe Illustrator 17 (CC 2014.1), Mac OS X 10.9.5
 	Contact:
 		Sergei Aleksandrov - <sergei.a.aleks@gmail.com>

	Disclaimer: This script was written by a rapid and contains many bugs as I do not like adobe api and javascript.

*/

var doc,
	docArtboards,
	docArtboardSelected,
	docName,
	docNameWithExt,
	docDir,
	docFilePath,
	outputFilePostfixDefault,
	outputPostfixDefault,
	outputDirDefault,
	exportDocName,
	exportFilePostfix,
	exportPostfix,
	exportDir,
	exportPdf,
	exportPdfb,
	exportPng,
	exportTiff,
	exportPngPPI,
	exportTiffResolution,
	exportTiffIZW,
	artboardRangeStart,
	artboardRangeEnd,
	bleedingOffset,
	currentUnit;


if ( app.documents.length > 0 ) {

	// Document
	doc = app.activeDocument;

	docArtboards = doc.artboards;
	docArtboardSelected = doc.artboards.getActiveArtboardIndex()+1;

	docName = (/(.*)\.[^.]+$/).exec(doc.name)[1];
	docNameWithExt = doc.name;

	docDir = doc.fullName.parent.fsName;
	docFilePath = doc.fullName.fsName;


	// Default Output dirs
	outputFilePostfixDefault = "_print";
	outputPostfixDefault = "/release/print/";
	outputDirDefault = docDir+outputPostfixDefault;


	// Export configurations
	exportDocName = docName;

	exportFilePostfix = outputFilePostfixDefault;
	exportPostfix = outputPostfixDefault;
	exportDir = outputDirDefault;

	exportPdf = true;
	exportPdfb = true;
	exportPng = true;
	exportTiff = false; // tiff is incorrect, thanks Adobe again :((

	exportPngPPI = 300.0;

	exportTiffResolution = 300;
	exportTiffIZW = true;

	artboardRangeStart = setArtboartRange(docArtboards, 1, docArtboards.length).start; // from 1
	artboardRangeEnd = setArtboartRange(docArtboards, 1, docArtboards.length).end; // to

	bleedingOffset = 3;

	currentUnit = getUnit();

	showGUI();

} else {
	alert("You must open at least one document.");
}


function showGUI () {

	box = new Window('dialog', "Print export settings: "+doc.name);

// Configurations --------------------------------------------------------------------------------------
	box.panel = box.add('panel', undefined, "Configurations:");
	box.panel.alignChildren = 'left';
	box.panel.panel0 = box.panel.add('panel', undefined, "Naming:");
	box.panel.panel0.alignChildren = 'left';
	box.panel.panel0.group0 = box.panel.panel0.add('group', undefined );
	box.panel.panel0.group0.orientation = 'row';
	box.panel.panel0.group0.text1 = box.panel.panel0.group0.add('statictext', undefined, "Basic name + postfix:");
	box.panel.panel0.group0.text2 = box.panel.panel0.group0.add('edittext', undefined, exportDocName);
	box.panel.panel0.group0.text2.minimumSize = [80,0];
	box.panel.panel0.group0.text3 = box.panel.panel0.group0.add('edittext', undefined, exportFilePostfix);
	box.panel.panel0.group0.text3.minimumSize = [80,0];
	box.panel.panel0.group0.changeBtn2 = box.panel.panel0.group0.add('button',undefined, "Default");

	// Folders
	box.panel.panel1 = box.panel.add('panel', undefined, "Folders:");
	box.panel.panel1.alignChildren = 'left';
	box.panel.panel1.group = box.panel.panel1.add('group', undefined );
	box.panel.panel1.group.orientation = 'row';
	box.panel.panel1.group.text1 = box.panel.panel1.group.add('statictext', undefined, "Release dir:");
	box.panel.panel1.group.text2 = box.panel.panel1.group.add('edittext', undefined, outputDirDefault);
	box.panel.panel1.group.text2.maximumSize = [300,27];
	box.panel.panel1.group.changeBtn = box.panel.panel1.group.add('button',undefined, "Change");
	box.panel.panel1.group.changeBtn2 = box.panel.panel1.group.add('button',undefined, "Default");
	box.panel.group0 = box.panel.add('group', undefined );
	box.panel.group0.orientation = 'row';
	box.panel.group0.alignChildren = 'top';

	// Format options
	box.panel.group0.panel2 = box.panel.group0.add('panel', undefined, "Format options:");
	box.panel.group0.panel2.alignChildren = 'left';
	box.panel.group0.panel2.group2 = box.panel.group0.panel2.add('group', undefined );
	box.panel.group0.panel2.group2.orientation = 'row';
	box.panel.group0.panel2.group2.changeBtn2 = box.panel.group0.panel2.group2.add('Checkbox',undefined, "Pdf");
	box.panel.group0.panel2.group2.changeBtn2.value = exportPdf;
	box.panel.group0.panel2.group2.changeBtn3 = box.panel.group0.panel2.group2.add('Checkbox',undefined, "Pdf (bleed)");
	box.panel.group0.panel2.group2.changeBtn3.value = exportPdfb;
	box.panel.group0.panel2.group2.changeBtn4 = box.panel.group0.panel2.group2.add('Checkbox',undefined, "Png");
	box.panel.group0.panel2.group2.changeBtn4.value = exportPng;
	box.panel.group0.panel2.group2.changeBtn5 = box.panel.group0.panel2.group2.add('Checkbox',undefined, "Tiff");
	box.panel.group0.panel2.group2.changeBtn5.value = exportTiff;

	box.panel.group0.panel2.panel0 = box.panel.group0.panel2.add('panel', undefined, "Bleed options (for remove bleed):");
	box.panel.group0.panel2.panel0.alignChildren = 'left';
	box.panel.group0.panel2.panel0.group = box.panel.group0.panel2.panel0.add('group', undefined );
	box.panel.group0.panel2.panel0.group.orientation = 'row';
	box.panel.group0.panel2.panel0.group.text = box.panel.group0.panel2.panel0.group.add('statictext', undefined, "Bleed size:");
	box.panel.group0.panel2.panel0.group.text2 = box.panel.group0.panel2.panel0.group.add('edittext', undefined, bleedingOffset);
	box.panel.group0.panel2.panel0.group.text2.minimumSize = [60,0];
	box.panel.group0.panel2.panel0.group.text3 = box.panel.group0.panel2.panel0.group.add('statictext', undefined, currentUnit);


	box.panel.group0.panel2.panel1 = box.panel.group0.panel2.add('panel', undefined, "Png options:");
	box.panel.group0.panel2.panel1.alignChildren = 'left';
	box.panel.group0.panel2.panel1.group = box.panel.group0.panel2.panel1.add('group', undefined );
	box.panel.group0.panel2.panel1.group.orientation = 'row';

	box.panel.group0.panel2.panel1.group.text = box.panel.group0.panel2.panel1.group.add('statictext', undefined, "PPI:");
	box.panel.group0.panel2.panel1.group.text2 = box.panel.group0.panel2.panel1.group.add('edittext', undefined, exportPngPPI);
	box.panel.group0.panel2.panel1.group.text2.minimumSize = [60,0];
	box.panel.group0.panel2.panel2 = box.panel.group0.panel2.add('panel', undefined, "Tiff options:");
	box.panel.group0.panel2.panel2.alignChildren = 'left';
	box.panel.group0.panel2.panel2.changeBtn2 = box.panel.group0.panel2.panel2.add('Checkbox',undefined, "IZWCompression");
	box.panel.group0.panel2.panel2.changeBtn2.value = exportTiffIZW;
	box.panel.group0.panel2.panel2.group = box.panel.group0.panel2.panel2.add('group', undefined );
	box.panel.group0.panel2.panel2.group.orientation = 'row';
	box.panel.group0.panel2.panel2.group.text = box.panel.group0.panel2.panel2.group.add('statictext', undefined, "Resolution:");
	box.panel.group0.panel2.panel2.group.text2 = box.panel.group0.panel2.panel2.group.add('edittext', undefined, exportTiffResolution);
	box.panel.group0.panel2.panel2.group.text2.minimumSize = [60,0];

	// Artboard range
	box.panel.group0.panel = box.panel.group0.add('panel', undefined, "Artboard range:");
	box.panel.group0.panel.alignChildren = 'left';
	box.panel.group0.panel.group3 = box.panel.group0.panel.add('group', undefined );
	box.panel.group0.panel.group3.orientation = 'row';
	box.panel.group0.panel.group3.text = box.panel.group0.panel.group3.add('statictext', undefined, "Artboards:");
	box.panel.group0.panel.group3.text2 = box.panel.group0.panel.group3.add('statictext', undefined, docArtboards.length);
	box.panel.group0.panel.group3.text3 = box.panel.group0.panel.group3.add('statictext', undefined, "Selected artboard:");
	box.panel.group0.panel.group3.text4 = box.panel.group0.panel.group3.add('statictext', undefined, docArtboardSelected);
	box.panel.group0.panel.group4 = box.panel.group0.panel.add('group', undefined );
	box.panel.group0.panel.group4.orientation = 'row';
	box.panel.group0.panel.group4.text = box.panel.group0.panel.group4.add('statictext', undefined, "From:");
	box.panel.group0.panel.group4.text2 = box.panel.group0.panel.group4.add('edittext', undefined, artboardRangeStart);
	box.panel.group0.panel.group4.text2.minimumSize = [40,0];
	box.panel.group0.panel.group4.text3 = box.panel.group0.panel.group4.add('statictext', undefined, "To:");
	box.panel.group0.panel.group4.text4 = box.panel.group0.panel.group4.add('edittext', undefined, artboardRangeEnd);
	box.panel.group0.panel.group4.text4.minimumSize = [40,0];
	box.panel.group0.panel.group4.allBtn = box.panel.group0.panel.group4.add('button',undefined, "All");
	box.panel.group0.panel.group4.allBtn.maximumSize = [40,27];
	box.panel.group0.panel.group4.curBtn = box.panel.group0.panel.group4.add('button',undefined, "Current");
	box.panel.group0.panel.group4.curBtn.maximumSize = [60,27];


	// Export info panel -----------------------------------------------------------------------------------
	box.exportStatPanel = box.add('panel', undefined, "Export info:");
	box.exportStatPanel.alignChildren = 'left';
	box.exportStatPanel.group6 = box.exportStatPanel.add('group', undefined );
	box.exportStatPanel.group6.orientation = 'row';
	box.exportStatPanel.group6.text = box.exportStatPanel.group6.add('statictext', undefined, "Basic name:");
	box.exportStatPanel.group6.text1 = box.exportStatPanel.group6.add('statictext', undefined, exportDocName+exportFilePostfix);
	box.exportStatPanel.group6.text1.minimumSize = [300,0];
	box.exportStatPanel.group5 = box.exportStatPanel.add('group', undefined );
	box.exportStatPanel.group5.orientation = 'row';
	box.exportStatPanel.group5.text4 = box.exportStatPanel.group5.add('statictext', undefined, "Directory:");
	box.exportStatPanel.group5.text5 = box.exportStatPanel.group5.add('statictext', undefined, "."+exportPostfix);
	box.exportStatPanel.group7 = box.exportStatPanel.add('group', undefined );
	box.exportStatPanel.group7.orientation = 'row';
	box.exportStatPanel.group7.text1 = box.exportStatPanel.group7.add('statictext', undefined, "Formats:");
	box.exportStatPanel.group7.text2 = box.exportStatPanel.group7.add('statictext', undefined, "PDF");
	box.exportStatPanel.group7.text2.enabled = exportPdf;
	box.exportStatPanel.group7.text3 = box.exportStatPanel.group7.add('statictext', undefined, "PDF(B)");
	box.exportStatPanel.group7.text3.enabled = exportPdfb;
	box.exportStatPanel.group7.text4 = box.exportStatPanel.group7.add('statictext', undefined, "PNG");
	box.exportStatPanel.group7.text4.enabled = exportPng;
	box.exportStatPanel.group7.text5 = box.exportStatPanel.group7.add('statictext', undefined, "TIFF");
	box.exportStatPanel.group7.text5.enabled = exportTiff;

	box.exportStatPanel.group8 = box.exportStatPanel.add('group', undefined );
	box.exportStatPanel.group8.orientation = 'row';
	box.exportStatPanel.group8.text = box.exportStatPanel.group8.add('statictext', undefined, "Artboard range:");
	box.exportStatPanel.group8.text3 = box.exportStatPanel.group8.add('statictext', undefined, artboardRangeStart);
	box.exportStatPanel.group8.text3.minimumSize = [35,0];
	box.exportStatPanel.group8.text4 = box.exportStatPanel.group8.add('statictext', undefined, "-");
	box.exportStatPanel.group8.text4.enabled = false;
	box.exportStatPanel.group8.text5 = box.exportStatPanel.group8.add('statictext', undefined, artboardRangeEnd);
	box.exportStatPanel.group8.text5.minimumSize = [35,0];

	// Confirm ---------------------------------------------------------------------------------------------
	box.group99 = box.add('group', undefined );
	box.group99.orientation = 'row';
	box.group99.alignment = 'right';
	box.group99.cancel = box.group99.add('button',undefined, "Cancel");
	box.group99.cancel.alignment = 'right';
	box.group99.go = box.group99.add('button',undefined, "Export");
	box.group99.go.alignment = 'right';


	box.group99.cancel.onClick = function() {
		box.close();
	};
	box.group99.go.onClick = function() {
		runExport()
	};
	box.panel.panel0.group0.changeBtn2.onClick = function() {
		box.panel.panel0.group0.text2.text = docName;
		box.panel.panel0.group0.text3.text = outputFilePostfixDefault;
		box.exportStatPanel.group6.text1.text = docName+outputFilePostfixDefault;
	};
	box.panel.panel0.group0.text2.onChanging = function() {
		exportDocName = box.panel.panel0.group0.text2.text;
		box.exportStatPanel.group6.text1.text = exportDocName+exportFilePostfix;
	};
	box.panel.panel0.group0.text3.onChanging = function() {
		exportFilePostfix = box.panel.panel0.group0.text3.text;
		box.exportStatPanel.group6.text1.text = exportDocName+exportFilePostfix;
	};
	box.panel.panel1.group.changeBtn.onClick = function() {
		var dd = Folder.selectDialog( 'Select the export directory.', docDir );
		if (!dd) {
			dd = outputDirDefault;
		}
		exportDir = dd;
		box.panel.panel1.group.text2.text = exportDir;
		box.exportStatPanel.group5.text5.text = exportDir;
	};
	box.panel.panel1.group.changeBtn2.onClick = function() {
		exportDir = outputDirDefault;
		box.panel.panel1.group.text2.text = outputDirDefault;
		box.exportStatPanel.group5.text5.text = outputDirDefault
	};
	box.panel.panel1.group.text2.onChanging = function() {
		exportDir = box.panel.panel1.group.text2.text;
		box.exportStatPanel.group5.text5.text = exportDir;
	};
	box.panel.group0.panel2.group2.changeBtn2.onClick = function() {
		exportPdf = box.panel.group0.panel2.group2.changeBtn2.value;
		box.exportStatPanel.group7.text2.enabled = exportPdf;
		box.group99.go.enabled = !!(exportPdf || exportPdfb || exportPng || exportTiff);
	};
	box.panel.group0.panel2.group2.changeBtn3.onClick = function() {
		exportPdfb = box.panel.group0.panel2.group2.changeBtn3.value;
		box.exportStatPanel.group7.text3.enabled = exportPdfb;
		box.group99.go.enabled = !!(exportPdf || exportPdfb || exportPng || exportTiff);
	};
	box.panel.group0.panel2.group2.changeBtn4.onClick = function() {
		exportPng = box.panel.group0.panel2.group2.changeBtn4.value;
		box.exportStatPanel.group7.text4.enabled = exportPng;
		box.panel.group0.panel2.panel1.enabled = exportPng;
		box.group99.go.enabled = !!(exportPdf || exportPdfb || exportPng || exportTiff);
	};
	box.panel.group0.panel2.group2.changeBtn5.onClick = function() {
		exportTiff = box.panel.group0.panel2.group2.changeBtn5.value;
		box.exportStatPanel.group7.text5.enabled = exportTiff;
		box.panel.group0.panel2.panel2.enabled = exportTiff;
		box.group99.go.enabled = !!(exportPdf || exportPdfb || exportPng || exportTiff);
	};
	box.panel.group0.panel.group4.allBtn.onClick = function() {
		artboardRangeStart = setArtboartRange(docArtboards, 1, docArtboards.length).start;
		artboardRangeEnd = setArtboartRange(docArtboards, 1, docArtboards.length).end;

		box.panel.group0.panel.group4.text2.text = artboardRangeStart;
		box.panel.group0.panel.group4.text4.text = artboardRangeEnd;
		box.exportStatPanel.group8.text3.text = artboardRangeStart;
		box.exportStatPanel.group8.text5.text = artboardRangeEnd;
	};
	box.panel.group0.panel.group4.curBtn.onClick = function() {
		artboardRangeStart = setArtboartRange(docArtboards, docArtboardSelected,docArtboardSelected).start;
		artboardRangeEnd = setArtboartRange(docArtboards, docArtboardSelected,docArtboardSelected).end;
		box.panel.group0.panel.group4.text2.text = artboardRangeStart;
		box.panel.group0.panel.group4.text4.text = artboardRangeEnd;
		box.exportStatPanel.group8.text3.text = artboardRangeStart;
		box.exportStatPanel.group8.text5.text = artboardRangeEnd;
	};
	box.panel.group0.panel.group4.text2.onChanging = function() {
		artboardRangeStart = setArtboartRange(docArtboards, box.panel.group0.panel.group4.text2.text, artboardRangeEnd).start;
		box.exportStatPanel.group8.text3.text = artboardRangeStart;
	};
	box.panel.group0.panel.group4.text4.onChanging = function() {
		artboardRangeEnd = setArtboartRange(docArtboards, artboardRangeStart, box.panel.group0.panel.group4.text4.text).end;
		box.exportStatPanel.group8.text5.text = artboardRangeEnd;
	};
	box.panel.group0.panel2.panel1.group.text2.onChanging = function() {
		exportPngPPI = box.panel.group0.panel2.panel1.group.text2.text;
	};
	box.panel.group0.panel2.panel2.changeBtn2.onClick = function() {
		exportTiffIZW = box.panel.group0.panel2.panel2.changeBtn2.value;
	};
	box.panel.group0.panel2.panel2.group.text2.onChanging = function() {
		exportTiffResolution = box.panel.group0.panel2.panel2.group.text2.text;
	};

	box.panel.group0.panel2.panel0.group.text2.onChanging = function() {
		bleedingOffset = box.panel.group0.panel2.panel0.group.text2.text;
	};

	box.panel.group0.panel2.panel1.enabled = exportPng;
	box.panel.group0.panel2.panel2.enabled = exportTiff;

	box.center();
	box.show();

}

function runExport() {

	//Make env
	makeDir(exportDir);

	// Main export
	if (exportPdf || exportPdfb) {

		// Save ai with print convert
		saveAi(exportDir, exportDocName + exportFilePostfix);
		recurseUnlockLayers( doc.layers ); // unlock and convert text to outlines
		saveAi(exportDir, exportDocName + exportFilePostfix);

		if (exportPdf) {
			setArtboardSize(-bleedingOffset, artboardRangeStart, artboardRangeEnd); // remove bleed <-- this hack. Thanks Adobe =((
			exportPrintPdf(exportDir, exportDocName + exportFilePostfix,
				false,
				artboardRangeStart, artboardRangeEnd
			);
			setArtboardSize(bleedingOffset, artboardRangeStart, artboardRangeEnd); // restore bleed
		}

		if (exportPdfb) {
			exportPrintPdf(exportDir, exportDocName + exportFilePostfix + "(withbleed)",
				true,
				artboardRangeStart, artboardRangeEnd
			);
		}

	}

	// Raster export
	if (exportPng) {
		exportPrintPng(exportDir, exportDocName + exportFilePostfix,
			exportPngPPI,
			artboardRangeStart,artboardRangeEnd
		);
	}

	if (exportTiff) {
		setArtboardSize(-bleedingOffset, artboardRangeStart,artboardRangeEnd); // remove bleed
		exportPrintTiff(exportDir, exportDocName + exportFilePostfix,
			exportTiffResolution, exportTiffIZW,
			artboardRangeStart,artboardRangeEnd
		);
		setArtboardSize(bleedingOffset, artboardRangeStart,artboardRangeEnd); // restore bleed
	}

	// Reload
	if (exportPdf || exportPdfb) {
		reload(docFilePath);
	}

	alert("Done");

}

function reload(path) {

	// close temp file
	doc.close(SaveOptions.DONOTSAVECHANGES);

	// open user document
	open(new File(path), DocumentColorSpace.CMYK);
}

function makeDir(path) {
	var newFolder = new Folder(path);
	newFolder.create();
}

function saveAi(path, name) {

	var newFile = new File(path + "/" + name + ".ai");

	var saveOptions = new IllustratorSaveOptions();
	saveOptions.compatibility = Compatibility.ILLUSTRATOR17;
	saveOptions.cmykPostScript = true;
	saveOptions.embedAllFonts = true;

	doc.saveAs( newFile, saveOptions );
}

function exportPrintPdf(path, name, bleed, rstart, rend) {

	var destFile = new File(path + "/" + name + ".pdf");

	var saveOptions = new PDFSaveOptions();
	saveOptions.pDFPreset = '[Illustrator Default]';
	saveOptions.compatibility = PDFCompatibility.ACROBAT5;
	saveOptions.generateThumbnails = true;
	saveOptions.preserveEditability = true;

	if(bleed) {
		saveOptions.registrationMarks = true;
		saveOptions.trimMarks = true;
		saveOptions.pageInformation = true;
		saveOptions.colorBars = true;
	}

	var range = rstart +"-"+ rend;
	saveOptions.artboardRange = range.toString() ;

	doc.saveAs( destFile, saveOptions );
}

function exportPrintPng(path, name, dpi, rstart, rend) {

	for (var i = rstart-1; i < rend; i++ )	{

		var destFile = new File(path + "/" + name + "_" + (i+1) + ".png");

		var saveOptions = new ExportOptionsPNG24();
		var type = ExportType.PNG24;
		saveOptions.antiAliasing = false;
		saveOptions.transparency = false;
		saveOptions.artBoardClipping = true;


		var aWeightPt = doc.artboards[i].artboardRect[2] - doc.artboards[i].artboardRect[0];
		var aHeightPt = doc.artboards[i].artboardRect[1] - doc.artboards[i].artboardRect[3];

		// Searching scaling
		var dpiW = ( (((aWeightPt * 0.01389) * dpi).toFixed() / aWeightPt)*100 );
		var dpiH = ( (((aHeightPt * 0.01389) * dpi).toFixed() / aHeightPt)*100 );

		saveOptions.horizontalScale = dpiW;
		saveOptions.verticalScale = dpiH;

		doc.artboards.setActiveArtboardIndex(i);
		doc.exportFile( destFile, type, saveOptions );
	}

}

function exportPrintTiff(path, name, res, izw, rstart, rend) {

	for (var i = rstart-1; i < rend; i++ ) {

		var destFile = new File(path + "/" + name + ".tiff"); // <- don't work. Thanks Adobe =(

		var saveOptions = new ExportOptionsTIFF();
		var type = ExportType.TIFF;
		saveOptions.IZWCompression = izw;
		saveOptions.saveMultipleArtboards = true;

		saveOptions.resolution = res;
		saveOptions.artboardRange = (i+1).toString();

		doc.exportFile( destFile, type, saveOptions );
	}

}

function setArtboartRange(aboards, start, end) {

	if (start <= 0) {
		start = 1;
	} else if (start > end) {
		start = end;
	} else if (start > aboards.length) {
		start = end;
	}

	if (end <= 0) {
		end = start;
	} else if (end < start) {
		end = start;
	} else if (end > aboards.length) {
		end = aboards.length;
	}

	return {start: start, end: end};
}

function setArtboardSize(bleed, rstart, rend) {

	var currentUnitX = getUnitMul();

	// Resizing
	for (var i = rstart-1; i < rend; i++ ) {

		var artbBounds = doc.artboards[i].artboardRect; // current size artboard

		artbBounds[0] -= bleed * currentUnitX; //left coordinate (use negative values to add artboard)
		artbBounds[1] += bleed * currentUnitX; //ltop coordinate
		artbBounds[2] += bleed * currentUnitX; //right coordinate
		artbBounds[3] -= bleed * currentUnitX; //bottom coordinate (use negative values to add artboard)

		doc.artboards[i].artboardRect = artbBounds;
	}

}

function getUnitMul() {

	var currentUnitMul = 1;

	var mm = 2.834645; // Millimeters x2.834645
	var cm = 28.34645; // Centimeters x28.34645
	var inch = 72; // Inches x72
	var picas = 12; // picas x12

	if ( getUnit() == "mm") {
		currentUnitMul = mm;
	} else if ( getUnit() == "cm" ) {
		currentUnitMul = cm;
	} else if ( getUnit() == "inch" ) {
		currentUnitMul = inch;
	} else if ( getUnit() == "picas" ) {
		currentUnitMul = picas;
	}

	return currentUnitMul;
}

function getUnit() {

	var currentUnit;

	if (doc.rulerUnits == RulerUnits.Millimeters) {
		currentUnit = "mm";
	} else if (doc.rulerUnits == RulerUnits.Centimeters) {
		currentUnit = "cm";
	} else if (doc.rulerUnits == RulerUnits.Inches) {
		currentUnit = "inch";
	} else if (doc.rulerUnits == RulerUnits.Picas) {
		currentUnit = "picas";
	} else if (doc.rulerUnits == RulerUnits.Points) {
		currentUnit = "points";
	}

	return currentUnit;
}

function recurseUnlockLayers( objArray ) {
	// Copy from Adobe Community, thank <Muppet Mark>

	for ( var i = 0; i < objArray.length; i++ ) {

		// Record previous value with conditional change
		var l = objArray[i].locked;
		if ( l ) objArray[i].locked = false;

		// Record previous value with conditional change
		var v = objArray[i].visible;
		if ( !v ) objArray[i].visible = true;

		outlineText( objArray[i].textFrames );

		// Recurse the contained layer collection
		if ( objArray[i].layers.length > 0 ) {
			recurseUnlockLayers( objArray[i].layers )
		}

		// Recurse the contained group collection
		if ( objArray[i].groupItems.length > 0 ) {
			recurseUnlockGroups( objArray[i].groupItems )
		}

		// Return to previous values
		objArray[i].locked = l;
		objArray[i].visible = v;
	}
}

function recurseUnlockGroups( objArray ) {
	// Copy from Adobe Community, thank <Muppet Mark>

	for ( var i = 0; i < objArray.length; i++ ) {

		// Record previous value with conditional change
		var l = objArray[i].locked;
		if ( l ) objArray[i].locked = false;

		// Record previous value with conditional change
		var h = objArray[i].hidden;
		if ( h ) objArray[i].hidden = false;

		outlineText( objArray[i].textFrames );

		// Recurse the contained group collection
		if ( objArray[i].groupItems.length > 0 ) {
			recurseUnlockGroups( objArray[i].groupItems )
		}

		// Return to previous values
		objArray[i].locked = l;
		objArray[i].hidden = h;
	}
}

function outlineText( objArray ) {
	// Copy from Adobe Community, thank <Muppet Mark>

	// Reverse this loop as it brakes the indexing
	for ( var i = objArray.length-1; i >= 0; i-- ) {

		// Record previous value with conditional change
		var l = objArray[i].locked;
		if ( l ) objArray[i].locked = false;

		// Record previous value with conditional change
		var h = objArray[i].hidden;
		if ( h ) objArray[i].hidden = false;

		var g = objArray[i].createOutline(  );

		// Return new group to previous Text Frame values
		g.locked = l;
		g.hidden = h;

	}

}

// Don't use - contains bugs
function unlockAll () {

	// Unlock layers
	var warnLayerVisible = [];

	for(var i = 0; i <= doc.layers.length - 1; i++) {

		if(doc.layers[i].visible != true) {
			warnLayerVisible.push(doc.layers[i].name);
		}

		doc.layers[i].locked = false;
		doc.layers[i].visible = true;
	}

	if (warnLayerVisible.length > 0) {
		alert("WARN! Document contains hidden layers: "+warnLayerVisible);
	}

	//Unlock collection objs
	for(var x = 0; x <= doc.pageItems.length - 1; x++) {
		doc.pageItems[x].locked = false;
	}

	//Unlock group
	for(var z = 0; z <= doc.groupItems.length - 1; z++) {
		doc.groupItems[z].locked = false;
	}

	// Unlock all text
	for(var f = 0; f <= doc.textFrames.length - 1; f++) {
		var frame = doc.textFrames[f];
		frame.selected = false;
		frame.locked = false;
		frame.layer.locked = false;
	}
}

function convertAllTextToOutlines () {
	for(var i = 0; i <= doc.textFrames.length - 1; i++) {
		doc.textFrames[i].createOutline();
	}
}
