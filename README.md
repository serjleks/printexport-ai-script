# Export for printing (Adobe Illustrator)

Adobe Illustrator script.


## Description
- - -

Export for printing - Adobe Illustrator script to export files for printing.
Export formats: 'pdf' and with bleed, 'png', 'tiff'(experimental)

Attention: Tested in Adobe Illustrator 17 (CC 2014.1), Mac OS X 10.9.5


## Usage
- - -

### Just run: 
 
    Illustrator -> File -> Scripts -> Other Script...

### Install script:

- Copy script src/ExportForPrint.jsx to:
 
        Adobe Illustrator\Presets\en_GB\Scripts

- Restart Illustrator

- Run: 

        Illustrator -> File -> Scripts -> ExportForPrint

## Contact
- - -

* Sergei Aleksandrov - <sergei.a.aleks@gmail.com> 

## License
- - -

MIT is open-sourced software licensed under the MIT license.


## Changelog
- - -

### version 0.1.0
 - add GUI
 - add export formats 'pdf', 'png', 'tiff'(experimental)

### version 0.1.1
 - fix GUI length
 - remove incorrect gui elements
 - add bleed options

### version 0.1.2
 - hotfix basename
 